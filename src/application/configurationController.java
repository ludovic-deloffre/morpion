package application;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Scanner;

import ai.MultiLayerPerceptron;
import ai.SigmoidalTransferFunction;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;

public class configurationController implements Initializable{

	@FXML
    private ToggleButton facile;
    
    @FXML
    private ToggleButton moyen;
    
    @FXML
    private ToggleButton difficile;
    
    @FXML
    private ToggleGroup difficulties;

    @FXML
    private Button save;

    @FXML
    private Button cancel;
    
    @FXML
    private Slider lrSlider;
    
    @FXML
    private Slider hSlider;
    
    @FXML
    private Slider nbLayersSlider;
    
    @FXML
    private Text lrValue;
    
    @FXML
    private Text hValue;
    
    @FXML
    private Text nbLayersValue;

	@FXML
	private Text pourcentageText;

	@FXML
	private ProgressBar bar;
    
    private String oldLine = "";

    
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/application/config.txt"));
			String line = reader.readLine();
			
			while (line != null) {
				// read next line

//				System.out.println("Line : " + line);
				if(line.contains("F")) {
					String[] facile = line.split(":");
					hSlider.setValue(Double.parseDouble(facile[1]));
				    lrSlider.setValue(Double.parseDouble(facile[2]));
				    nbLayersSlider.setValue(Double.parseDouble(facile[3]));
				    oldLine = line;
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// h slider
		hSlider.setMin(128);
		hSlider.setMax(1024);

		hValue.setText(String.valueOf(hSlider.getValue()));
	    hSlider.setMajorTickUnit(128);
	    hSlider.setMinorTickCount(0);
	    hSlider.setSnapToTicks(true);
	    hSlider.setShowTickMarks(true);
	    hSlider.setShowTickLabels(true);
	    
	    // lr slider
	    lrSlider.setMin(0);
	    lrSlider.setMax(1);

	    lrValue.setText(String.valueOf(lrSlider.getValue()));
	    lrSlider.setMajorTickUnit(0.1);
	    lrSlider.setMinorTickCount(0);
	    lrSlider.setSnapToTicks(true);
	    lrSlider.setShowTickMarks(true);
	    lrSlider.setShowTickLabels(true);
	    
	    // nbLayers slider
	    nbLayersSlider.setMin(1);
	    nbLayersSlider.setMax(10);

	    nbLayersValue.setText(String.valueOf(nbLayersSlider.getValue()));
	    nbLayersSlider.setMajorTickUnit(1);
	    nbLayersSlider.setMinorTickCount(0);
	    nbLayersSlider.setSnapToTicks(true);
	    nbLayersSlider.setShowTickMarks(true);
	    nbLayersSlider.setShowTickLabels(true);

//	    bar.setProgress(0.4);
	}
    


	 @FXML
	 public void handleCloseButtonCancel(ActionEvent event) {
	     Stage stage = (Stage) cancel.getScene().getWindow();
	     stage.close();
	 }
	 
	 @FXML
	 public void handleCloseButtonSave(ActionEvent event) throws IOException {
		 saveConfig(event);

//		 if(saved == true) {
//			 Stage stage = (Stage) save.getScene().getWindow();
//			 stage.close();
//		 }
	 }

	 
	 @FXML
	 public void onLrSliderChanged() {
		 double sliderValue = (double) lrSlider.getValue();
		 lrValue.setText(String.valueOf(sliderValue));
	 }
	 
	 @FXML
	 public void onHSliderChanged() {
		 int sliderValue = (int) hSlider.getValue();
		 hValue.setText(String.valueOf(sliderValue));
	 }

	 
	 @FXML
	 public void onNbLayersSliderChanged() {
		 int sliderValue = (int) nbLayersSlider.getValue();
		 nbLayersValue.setText(String.valueOf(sliderValue));
	 }
	 
	 @FXML
	 public void loadDifficultyData() {
		 
		 ToggleButton selectedToggleButton = (ToggleButton) difficulties.getSelectedToggle();
//		 System.out.println("selectedToggleButton : " + selectedToggleButton.getId());
		 
		 String value = "";
		 if(selectedToggleButton.getId().contains("facile")) {
			 value = "F";
		 }
		 else if(selectedToggleButton.getId().contains("moyen")) {
			 value = "M";
		 }
		 else if(selectedToggleButton.getId().contains("difficile")) {
			 value = "D";
		 }
		 
		 
		 BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/application/config.txt"));
				String line = reader.readLine();
				
				while (line != null) {
					// read next line

//					System.out.println("Line : " + line);
					if(line.contains(value)) {
						String[] lineSplit = line.split(":");
					    hSlider.setValue(Integer.parseInt(lineSplit[1]));
					    hValue.setText(String.valueOf(hSlider.getValue()));
					    lrSlider.setValue(Double.parseDouble(lineSplit[2]));
					    lrValue.setText(String.valueOf(lrSlider.getValue()));
					    nbLayersSlider.setValue(Integer.parseInt(lineSplit[3]));
					    nbLayersValue.setText(String.valueOf(nbLayersSlider.getValue()));
					    oldLine = line;
					}
					line = reader.readLine();
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	 }
	 
	 @FXML
	    boolean saveConfig(ActionEvent event) throws IOException {
		 ToggleButton selectedToggleButton = (ToggleButton) difficulties.getSelectedToggle();

	      String filePath = System.getProperty("user.dir") + "/src/application/config.txt";
	      Scanner sc = new Scanner(new File(filePath));
	      StringBuffer buffer = new StringBuffer();
	      while (sc.hasNextLine()) {
	         buffer.append(sc.nextLine()+System.lineSeparator());
	      }
	      String fileContents = buffer.toString();
	      sc.close();

	      String newLine = "";
			 if(selectedToggleButton.getId().contains("facile")) {
				 newLine = "F:"+(int)  hSlider.getValue()+":"+lrSlider.getValue()+":"+ (int) nbLayersSlider.getValue();
			 }
			 else if(selectedToggleButton.getId().contains("moyen")) {
				 newLine = "M:"+(int)  hSlider.getValue()+":"+lrSlider.getValue()+":"+ (int) nbLayersSlider.getValue();
			 }
			 else if(selectedToggleButton.getId().contains("difficile")) {
				 newLine = "D:"+ (int) hSlider.getValue()+":"+lrSlider.getValue()+":"+ (int) nbLayersSlider.getValue();
			 }
			 System.out.println("oldLine : " + oldLine);
			 System.out.println("newLine : " + newLine);

	      fileContents = fileContents.replaceAll(oldLine, newLine);
	      //instantiating the FileWriter class
	      FileWriter writer = new FileWriter(filePath);
	      writer.append(fileContents);
	      writer.flush();
	      
	      
	      String fileSrl = System.getProperty("user.dir") + "/src/application/models/mlp_" + (int) hSlider.getValue() + "_" + lrSlider.getValue() + "_" + (int) nbLayersSlider.getValue() + ".srl";
	      int h = (int) hSlider.getValue();
	      double lr = lrSlider.getValue();
	      int l = (int) nbLayersSlider.getValue();

	      System.out.println(fileSrl);
	      File tmpFile = new File(fileSrl);
	      boolean exists = tmpFile.exists();
	      if(exists == false) {

//			  Non fonctionnel :

//			  test((int) hSlider.getValue(), lrSlider.getValue(), (int) nbLayersSlider.getValue(), fileSrl);
	    	  // mettre dans un thread
			  Task<Void> task = new Task<Void>() {
				  @Override protected Void call() {
					  System.out.println("h : " + h);
					  System.out.println("lr : " + lr);
					  System.out.println("l : " + l);


					  int[] layers = new int[l + 2];
					  layers[0] = 9;
					  for (int i = 0; i < l; i++) {
						  layers[i + 1] = h;
					  }
					  layers[layers.length - 1] = 9;


					  for (int i = 0; i < layers.length; i++) {
						  System.out.println(layers[i]);
					  }

					  try {

						  System.out.println();
						  System.out.println("START TRAINING ...");
						  System.out.println();

						  double error = 0.0;
						  MultiLayerPerceptron net = new MultiLayerPerceptron(layers, lr, new SigmoidalTransferFunction());
						  //				double epochs = 1000000 ;
						  double epochs = 100000;

						  System.out.println("---");
						  System.out.println("Load data ...");
						  HashMap<Integer, Coup> mapTrain = loadCoupsFromFile("./resources/train_dev_test/train.txt");
						  HashMap<Integer, Coup> mapDev = loadCoupsFromFile("./resources/train_dev_test/dev.txt");
						  HashMap<Integer, Coup> mapTest = loadCoupsFromFile("./resources/train_dev_test/test.txt");
						  System.out.println("---");
						  //TRAINING ...
						  int u = 0;
						  for (int i = 0; i < epochs; i++) {

							  Coup c = null;
							  while (c == null)
								  c = mapTrain.get((int) (Math.round(Math.random() * mapTrain.size())));

							  error += net.backPropagate(c.in, c.out);
							  float pourcentage = 0;
							  if (i % 100000 == 0) {
								  System.out.println("Error at step " + i + " is " + (error / (double) i));

								  //textArea.setText("Error at step "+i+" is "+ (error/(double)i)+"---> Pourcentage :"+ pourcentage);
//								  bar.setProgress(pourcentage / 100);
//								  pourcentageText.setText(String.valueOf(pourcentage) + " %");

								  //						bar.setProgress(0.7);
//								  System.out.println("Pourcentage : " + pourcentage);

								  u = i;
								  //String text = textArea.getText();
								  //System.out.println(text);
							  }
							  pourcentage = (float) ((i / epochs) * 100);
							  updateMessage(String.valueOf(pourcentage));
//							  pourcentageText.setText(String.valueOf(pourcentage) + " %");
							  updateProgress(i, epochs);
						  }


						  bar.setProgress(1);
						  error /= epochs;
						  //				textArea.setText("Error at step "+u+" is "+ error+"--->Pourcentage : 100");
						  if (epochs > 0)
							  System.out.println("Error is " + error);
						  //
						  System.out.println("Learning completed!");
						  pourcentageText.setText("Learning completed!");
						  net.save(fileSrl);
						  updateMessage("saved");

						  //TEST ...

					  } catch (Exception e) {
						  System.out.println("Test.test()");
						  e.printStackTrace();
						  System.exit(-1);
					  }
					  return null;
				  }
			  };

			  Thread t = new Thread(task);

			  t.start();

			  task.messageProperty().addListener(new ChangeListener<String>() {
				  @Override
				  public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				  	if(newValue.equals("saved")){
						 Stage stage = (Stage) save.getScene().getWindow();
						 stage.close();
					}
//				  	else{
						pourcentageText.setText(newValue + " %");
//					}
				  }
			  });

			  task.progressProperty().addListener(new ChangeListener<Number>() {
				  @Override
				  public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					  bar.setProgress((double) newValue);
				  }

			  });

//			  Fonctionnel
//			  Test2.test();



	      }
	      return false;
	 }




	public static HashMap<Integer, Coup> loadCoupsFromFile(String file){
		System.out.println("loadCoupsFromFile from "+file+" ...");
		HashMap<Integer, Coup> map = new HashMap<>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file))));
			String s = "";
			while ((s = br.readLine()) != null) {
				String[] sIn = s.split("\t")[0].split(" ");
				String[] sOut = s.split("\t")[1].split(" ");

				double[] in = new double[sIn.length];
				double[] out = new double[sOut.length];

				for (int i = 0; i < sIn.length; i++) {
					in[i] = new Double(sIn[i]);
				}

				for (int i = 0; i < sOut.length; i++) {
					out[i] = new Double(sOut[i]);
				}

				Coup c = new Coup(9, "");
				c.in = in ;
				c.out = out ;
				map.put(map.size(), c);
			}
			br.close();
		}
		catch (Exception e) {
			System.out.println("Test.loadCoupsFromFile()");
			e.printStackTrace();
			System.exit(-1);
		}
		return map ;
	}

}
