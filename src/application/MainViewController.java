package application;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

//import com.sun.glass.events.MouseEvent;

import ai.MultiLayerPerceptron;
import ai.SigmoidalTransferFunction;
import ai.Test;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class MainViewController {
	
	Task<Void> task = new Task<Void>() {
		int size = 9;
        @Override protected Void call(){
       	double epochs = 10000000 ;
		int[] layers = new int[]{ size, 128, size };
		double error = 0.0 ;
		MultiLayerPerceptron net = new MultiLayerPerceptron(layers, 0.1, new SigmoidalTransferFunction());
		HashMap<Integer, Coup> mapTrain = Test2.loadCoupsFromFile("./resources/train_dev_test/train.txt");

		
		int u = 0;
		for(int i = 0; i < epochs; i++){

			Coup c = null ;
			while ( c == null )
				c = mapTrain.get((int)(Math.round(Math.random() * mapTrain.size())));

			error += net.backPropagate(c.in, c.out);
			float pourcentage;
			if ( i % 10000 == 0 ) {
				pourcentage = (float) ((i/epochs)*100);
				updateMessage("Error at step "+i+" is "+ (error/(double)i)+"---> Pourcentage :"+ pourcentage);
			}
			updateProgress(i, epochs);
			u=i;
		}
		updateProgress(1, 1);
		updateMessage("Error at step "+u+" is "+ (error/(double)u)+"---> Pourcentage : 100");
		return null;
        }
    };

    @FXML
    private Button bouton1;

    @FXML
    private TextArea textArea;
    
    @FXML
    private ProgressBar bar;
    
    Thread t = new Thread(task);
   
    @FXML
    void launch(ActionEvent event) {
    	t.setDaemon(true);
    	t.start();
    	
    	bar.progressProperty().unbind();
    	textArea.cacheProperty().unbind();
    	//bar.progressProperty().bind(task.progressProperty());
    	
    	
    	task.messageProperty().addListener(new ChangeListener<String>() {
    		@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
    			textArea.setText(newValue);
				
			}
    	});
    	
    	task.progressProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				bar.setProgress((double) newValue);				
			}
    		
    	});
    }
    
   public void switchJouerSeul(ActionEvent event) throws IOException {

		Parent tableViewParent = FXMLLoader.load(getClass().getResource("jouerSeul.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);
		
		// Get the stage information
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		
		window.setScene(tableViewScene);
		window.show();
	   
		
   }
   
   public void switchJouerDeux(ActionEvent event) throws IOException {

		Parent tableViewParent = FXMLLoader.load(getClass().getResource("jouerDeux.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);
		
		// Get the stage information
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		
		window.setScene(tableViewScene);
		window.show();
	   
   }
   
   public void switchReglesSolo(ActionEvent event) throws IOException {

		Parent tableViewParent = FXMLLoader.load(getClass().getResource("reglesSolo.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);
		
		// Get the stage information
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		
		window.setScene(tableViewScene);
		window.show();
	   
  }
   
   public void switchReglesDeux(ActionEvent event) throws IOException {

		Parent tableViewParent = FXMLLoader.load(getClass().getResource("reglesDeux.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);
		
		// Get the stage information
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		
		window.setScene(tableViewScene);
		window.show();
	   
  }
   
   public void retourAcceuil(ActionEvent event) throws IOException {

		Parent tableViewParent = FXMLLoader.load(getClass().getResource("acceuil.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);
		
		// Get the stage information
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		
		window.setScene(tableViewScene);
		window.show();
	   
  }
   
   public void switchConfiguration(ActionEvent event) {
       try {
           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("configuration.fxml"));
           Parent root1 = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           stage.setScene(new Scene(root1));  
           stage.show();
       } catch (IOException e) {
           e.printStackTrace();
       }
   
   }
   
   public void switchModele(ActionEvent event) {
       try {
           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("selectModel.fxml"));
           Parent root2 = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           stage.setScene(new Scene(root2));  
           stage.show();
       } catch (IOException e) {
           e.printStackTrace();
       }
   }
   
   

   
   

   
   
   

}
