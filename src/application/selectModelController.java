package application;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

//import com.sun.glass.events.MouseEvent;

import ai.MultiLayerPerceptron;
import ai.SigmoidalTransferFunction;
import ai.Test;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class  selectModelController implements Initializable{
	private int file=0;
	ObservableList<String> list = FXCollections.observableArrayList();
	
	 @FXML
	 private ChoiceBox<String> choiceBox;
	 
	 @FXML
	 private Button delete;
	 
	 @FXML
	 private Button choose;
	 
	 @FXML
	 private Button save;
	 
	 @FXML
	 private Button cancel;
	 
	 
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {
		  loadData();
		  choiceBox.getSelectionModel().selectedIndexProperty().addListener(
			         (ObservableValue<? extends Number> ov, Number old_val, Number new_val) -> {
			            file=(int) new_val;
			      });
	 }
	 
	 
   public void listFilesForFolder(final File folder) {
	   list.removeAll(list);

	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry);
	        } else {
	            System.out.println(fileEntry.getName());
	   		 	list.addAll(fileEntry.getName());
	        }
	        
	    }
	    choiceBox.getItems().addAll(list);
	}
   
   public void listFilesForId(final File folder, int id) {
	   System.out.println("saluce");
	   
	   int cpt = 0;
	    for (final File fileEntry : folder.listFiles()) {
	    	System.out.println(id);
	    	System.out.println(cpt);
	        if(id == cpt) {
	        	deleteModelFile(fileEntry);
	        }
	        cpt++;
	    }
	}
   
   	public void deleteModelFile(final File file) {
   		if(file.delete()) {
   			System.out.println(file.getName() + " deleted");
   		}
   		else {
   			System.out.println("fail to delete : " + file.getName());  
   		}
   	}

	 
	 private void loadData() {
	   	String currentDirectory = System.getProperty("user.dir");
	   	System.out.println(currentDirectory);
		final File folder = new File(currentDirectory + "/src/application/models");
		
		//fichier temporaire
		String fileName = "mlp_128_0.1_3.srl";
		// mettre en argument le fichier selectionne 
		File fileToDelete = new File(currentDirectory + "/src/application/models/" + fileName);
		listFilesForFolder(folder);
//		deleteModelFile(fileToDelete);
		 
		 
		 
	 }
	 
	 @FXML
	    void deleteList(ActionEvent event) {
		 
		 	String currentDirectory = System.getProperty("user.dir");
		   	System.out.println(currentDirectory);
			final File folder = new File(currentDirectory + "/src/application/models");
			
			//fichier temporaire
			String fileName = "mlp_128_0.1_3.srl";
			// mettre en argument le fichier selectionne 
			File fileToDelete = new File(currentDirectory + "/src/application/models/" + fileName);
			listFilesForId(folder, file);
		 	System.out.println("depuis deleteList : " + file);

		 	list.remove(file);
		 	choiceBox.getItems().clear();
		 	choiceBox.getItems().addAll(list);
		 	
		   	

	 }
	 
	 @FXML
	 public void handleCloseButtonChoose(ActionEvent event) {
	     Stage stage = (Stage) choose.getScene().getWindow();
	     stage.close();
	 }
	 
	 @FXML
	 public void handleCloseButtonSave(ActionEvent event) {
	     Stage stage = (Stage) save.getScene().getWindow();
	     stage.close();
	 }
	 
	 @FXML
	 public void handleCloseButtonCancel(ActionEvent event) {
	     Stage stage = (Stage) cancel.getScene().getWindow();
	     stage.close();
	 }
	 
	 @FXML
	    void chooseModel(ActionEvent event) {
		 handleCloseButtonChoose(event);
		 // recuperer le fichier selectionne
	 }
	 

	 
	 
	 
	 

	 
	 
	 
	 

}
