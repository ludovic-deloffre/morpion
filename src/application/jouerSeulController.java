package application;



import java.awt.Label;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import ai.MultiLayerPerceptron;
import ai.SigmoidalTransferFunction;
import com.sun.javafx.geom.Rectangle;


import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ToggleGroup;

import javafx.stage.Stage;
import javafx.util.Duration;

public class jouerSeulController implements Initializable{

	@FXML
	private ToggleGroup difficulties;

	@FXML
	private Text case0,case1,case2,case3,case4,case5,case6,case7,case8,msgJoueur,scoreVert,scoreBleu;

	@FXML
	private Pane pane;

	private boolean inverse = false;

	private boolean testCase0=true, testCase1=true, testCase2=true, testCase3=true, testCase4=true, testCase5=true, testCase6=true, testCase7=true, testCase8=true;

	double[] inputs;

	int h = 0;
	double lr = 0.0;
	int l = 0;

	public void initialize(URL arg0, ResourceBundle arg1) {
		changeDifficulty();
		inputs = new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		msgJoueur.setText("C'est au tour du joueur !");
		setColor();
	}
	@FXML
	void retourAcceuil(ActionEvent event) throws IOException {
		Parent tableViewParent = FXMLLoader.load(getClass().getResource("acceuil.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);

		// Get the stage information
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(tableViewScene);
		window.show();
	}

	@FXML
	void switchReglesSolo(ActionEvent event) throws IOException {
		Parent tableViewParent = FXMLLoader.load(getClass().getResource("reglesSolo.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);

		// Get the stage information
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

		window.setScene(tableViewScene);
		window.show();



	}


	public void actionCase0(MouseEvent event){
		if(testCase0) {
			if(inverse) {
				case0.setText("X");
				inverse=false;
				testCase0=false;
				verifGagnant();
			}
			else {
				case0.setText("O");
				inputs[0] = 1;
				inverse=true;
				testCase0=false;
				verifGagnant();
				coupIA(event);
			}
			
		}

		//Line line = new Line(442, 242, 442, 530);
		//Line line = new Line(case0.getLayoutX()+75, case0.getLayoutY()-45, 442, 530);
		//line.setStartX(442.0f);
		//line.setStartY(242.0f);
		//line.setEndX(442.0f);
		//line.setEndY(530.0f);


	}

	public void actionCase1(MouseEvent event){
		if(testCase1) {
			if(inverse) {
				case1.setText("X");
				testCase1=false;
				inverse=false;
				verifGagnant();
				
			}
			else {
				case1.setText("O");
				inputs[1] = 1;
				inverse=true;
				testCase1=false;
				verifGagnant();
				coupIA(event);
			}
			
		}


	}

	public void actionCase2(MouseEvent event){
		if(testCase2) {
			if(inverse) {
				case2.setText("X");
				inverse=false;
				testCase2=false;
				verifGagnant();
			}
			else {
				case2.setText("O");
				inputs[2] = 1;
				inverse=true;
				
				testCase2=false;
				verifGagnant();
				coupIA(event);
			}
			
			
		}
	}

	public void actionCase3(MouseEvent event){
		if(testCase3) {
			if(inverse) {
				case3.setText("X");
				testCase3=false;
				inverse=false;
				verifGagnant();
				
			}
			else {
				case3.setText("O");
				inputs[3] = 1;
				inverse=true;
				
				testCase3=false;
				verifGagnant();
				coupIA(event);
			}
		
		}
	}

	public void actionCase4(MouseEvent event){
		if(testCase4) {
			if(inverse) {
				case4.setText("X");
				testCase4=false;
				inverse=false;
				verifGagnant();
				
			}
			else {
				case4.setText("O");
				inputs[4] = 1;
				inverse=true;
				
				testCase4=false;
				verifGagnant();
				coupIA(event);
			}
			
		}

	}

	public void actionCase5(MouseEvent event){
		if(testCase5) {
			if(inverse) {
				case5.setText("X");
				inverse=false;
				testCase5=false;
				verifGagnant();
			}
			else {
				case5.setText("O");
				inputs[5] = 1;
				inverse=true;
				testCase5=false;
				verifGagnant();
				coupIA(event);
			}
			
		}

	}

	public void actionCase6(MouseEvent event){
		if(testCase6) {
			if(inverse) {
				case6.setText("X");
				inverse=false;
				testCase6=false;
				verifGagnant();
			}
			else {
				case6.setText("O");
				inputs[6] = 1;
				inverse=true;
				testCase6=false;
				verifGagnant();
				coupIA(event);
			}
		}

	}
	public void actionCase7(MouseEvent event){
		if(testCase7) {
			if(inverse) {
				case7.setText("X");
				inverse=false;
				testCase7=false;
				verifGagnant();
			}
			else {
				case7.setText("O");
				inputs[7] = 1;
				inverse=true;
				
				testCase7=false;
				verifGagnant();
				coupIA(event);
			}
			
		}

	}

	public void actionCase8(MouseEvent event){
		if(testCase8) {
			if(inverse) {
				case8.setText("X");
				inverse=false;
				testCase8=false;
				verifGagnant();
			}
			else {
				case8.setText("O");
				inputs[8] = 1;
				inverse=true;
				
				testCase8=false;
				verifGagnant();
				coupIA(event);
			}
		
		}
	}

	public void setAllFalse() {
		testCase0=false;
		testCase1=false;
		testCase2=false;
		testCase3=false;
		testCase4=false;
		testCase5=false;
		testCase6=false;
		testCase7=false;
		testCase8=false;
	}

	public void setAllTrue() {
		testCase0=true;
		testCase1=true;
		testCase2=true;
		testCase3=true;
		testCase4=true;
		testCase5=true;
		testCase6=true;
		testCase7=true;
		testCase8=true;
		case0.setText("");
		case1.setText("");
		case2.setText("");
		case3.setText("");
		case4.setText("");
		case5.setText("");
		case6.setText("");
		case7.setText("");
		case8.setText("");
	}

	public void verifGagnant(){
		if((case0.getText()=="X") && (case3.getText()=="X") && (case6.getText()=="X")) {
			setAllFalse();
			Line line = new Line(442, 235, 442, 530);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);



		}
		else if((case1.getText()=="X") && (case4.getText()=="X") && (case7.getText()=="X")) {
			setAllFalse();
			Line line = new Line(597, 235, 597, 530);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case2.getText()=="X") && (case5.getText()=="X") && (case8.getText()=="X")) {
			setAllFalse();
			Line line = new Line(750, 235, 750, 530);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case0.getText()=="X") && (case1.getText()=="X") && (case2.getText()=="X")) {
			setAllFalse();
			Line line = new Line(442, 235, 750, 235);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case3.getText()=="X") && (case4.getText()=="X") && (case5.getText()=="X")) {
			setAllFalse();
			Line line = new Line(442, 380, 750, 380);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case6.getText()=="X") && (case7.getText()=="X") && (case8.getText()=="X")) {
			setAllFalse();
			Line line = new Line(442, 533, 750, 533);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case0.getText()=="X") && (case4.getText()=="X") && (case8.getText()=="X")) {
			Line line = new Line(442, 235, 750, 533);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			setAllFalse();
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case2.getText()=="X") && (case4.getText()=="X") && (case6.getText()=="X")) {
			Line line = new Line(442, 530, 750, 235);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			setAllFalse();
			msgJoueur.setText("La machine a gagn�!");
			int a = Integer.valueOf(scoreBleu.getText());
			scoreBleu.setText(String.valueOf(a+1));
			reload(line);
		}


		else if((case0.getText()=="O") && (case3.getText()=="O") && (case6.getText()=="O")) {
			setAllFalse();
			Line line = new Line(442, 235, 442, 530);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case1.getText()=="O") && (case4.getText()=="O") && (case7.getText()=="O")) {
			setAllFalse();
			Line line = new Line(597, 235, 597, 530);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case2.getText()=="O") && (case5.getText()=="O") && (case8.getText()=="O")) {
			setAllFalse();
			Line line = new Line(750, 235, 750, 530);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case0.getText()=="O") && (case1.getText()=="O") && (case2.getText()=="O")) {
			setAllFalse();
			Line line = new Line(442, 235, 750, 235);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case3.getText()=="O") && (case4.getText()=="O") && (case5.getText()=="O")) {
			setAllFalse();
			Line line = new Line(442, 380, 750, 380);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case6.getText()=="O") && (case7.getText()=="O") && (case8.getText()=="O")) {
			setAllFalse();
			Line line = new Line(442, 533, 750, 533);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case0.getText()=="O") && (case4.getText()=="O") && (case8.getText()=="O")) {
			Line line = new Line(442, 235, 750, 533);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			setAllFalse();
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if((case2.getText()=="O") && (case4.getText()=="O") && (case6.getText()=="O")) {
			Line line = new Line(442, 530, 750, 235);
			line.setStrokeWidth(4);
			line.setStroke(Paint.valueOf("RED"));
			pane.getChildren().addAll(line);
			setAllFalse();
			msgJoueur.setText("Le joueur vert a gagn�!");
			int a = Integer.valueOf(scoreVert.getText());
			scoreVert.setText(String.valueOf(a+1));
			reload(line);
		}
		else if(testCase0==false&&testCase1==false&&testCase2==false&&testCase3==false&&testCase4==false&&testCase5==false&&testCase6==false&&testCase7==false&&testCase8==false){
			Line line = new Line(0,0,0,0);
			msgJoueur.setText("Egalit� !");
			reload(line);
		}


		else {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				msgJoueur.setText("C'est au tour de la machine !");
			
			}
			else {
				msgJoueur.setText("C'est au tour du joueur !");
			}





			setColor();
		}

	}


	public void setColor() {
		
		if(testCase0) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case0.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case0.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase1) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case1.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case1.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase2) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case2.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case2.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase3) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case3.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case3.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase4) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case4.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case4.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase5) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case5.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case5.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase6) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case6.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case6.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase7) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case7.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case7.setFill(Paint.valueOf("#114dbc"));
			}
		}
		if(testCase8) {
			if(msgJoueur.getText().equals("C'est au tour du joueur !")) {
				case8.setFill(Paint.valueOf("#035e0c"));
			}
			else{
				case8.setFill(Paint.valueOf("#114dbc"));
			}
		}
	}

	public void reload(Line line) {
		Thread t = new Thread() {
			public void run() {
				try {
					Thread.sleep(2050);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				line.setStartX(0.0f);
				line.setStartY(0.0f);
				line.setEndX(0.0f);
				line.setEndY(0.0f);
				setAllTrue();
				inverse=false;
				msgJoueur.setText("C'est au tour du joueur !");
				setColor();
				inputs = new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
				

			}
		};

		t.start();


	}

	@FXML
	public void changeDifficulty() {

		ToggleButton selectedToggleButton = (ToggleButton) difficulties.getSelectedToggle();

		String value = "";
		if(selectedToggleButton.getId().contains("facile")) {
			value = "F";
		}
		else if(selectedToggleButton.getId().contains("moyen")) {
			value = "M";
		}
		else if(selectedToggleButton.getId().contains("difficile")) {
			value = "D";
		}

		BufferedReader reader;

		try {
			reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/application/config.txt"));
			String line = reader.readLine();



			while (line != null) {
				// read next line

//					System.out.println("Line : " + line);
				if(line.contains(value)) {
					String[] lineSplit = line.split(":");
					h = Integer.parseInt(lineSplit[1]);
					lr = Double.parseDouble(lineSplit[2]);
					l = Integer.parseInt(lineSplit[3]);
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String fileSrl = System.getProperty("user.dir") + "/src/application/models/mlp_" +h+ "_" +lr+ "_" +l+ ".srl";
		System.out.println(fileSrl);

		int[] layers = new int[l+2];
		layers[0] = 9;
		for(int i=0; i<l; i++){
			layers[i+1] = h;
		}
		layers[layers.length-1] = 9;

		MultiLayerPerceptron net = new MultiLayerPerceptron(layers, lr, new SigmoidalTransferFunction());
		net.load(fileSrl);

	}

	@FXML
	public void coupIA(MouseEvent event){
		
		
		int[] layers = new int[l+2];
		layers[0] = 9;
		for(int i=0; i<l; i++){
			layers[i+1] = h;
		}
		layers[layers.length-1] = 9;

		MultiLayerPerceptron net = new MultiLayerPerceptron(layers, lr, new SigmoidalTransferFunction());

		double[] output = net.forwardPropagation(inputs);

		double max = 0;
		int indice = 0;
		boolean iaPlayed = false;
		ArrayList<Integer> visited = new ArrayList<Integer>();
		while(iaPlayed == false) {

			for (int i = 0; i < 8; i++) {
				if (output[i] > max) {
					if(inputs[i] == 0) {
						max = output[i];
						indice = i;
					}
				}
			}

			System.out.println("Max : " + max);
			inputs[indice] = -1;
			if(indice == 0){
				actionCase0(event);
			}
			else if(indice == 1){
				actionCase1(event);
			}
			else if(indice == 2){
				actionCase2(event);
			}
			else if(indice == 3){
				actionCase3(event);
			}
			else if(indice == 4){
				actionCase4(event);
			}
			else if(indice == 5){
				actionCase5(event);
			}
			else if(indice == 6){
				actionCase6(event);
			}
			else if(indice == 7){
				actionCase7(event);
			}
			else if(indice == 8){
				actionCase8(event);
			}
			iaPlayed = true;
		}
	}



}
