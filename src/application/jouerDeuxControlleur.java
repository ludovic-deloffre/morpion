package application;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class jouerDeuxControlleur {
	    @FXML
	    private ToggleGroup difficultiesButton;
	    
	    @FXML
	    private Text case0,case1,case2,case3,case4,case5,case6,case7,case8,msgJoueur,scoreVert,scoreBleu;
	    
	    @FXML
	    private Pane pane;
	    
	    private boolean inverse = false;
	    
	    private boolean testCase0=true, testCase1=true, testCase2=true, testCase3=true, testCase4=true, testCase5=true, testCase6=true, testCase7=true, testCase8=true;
	    
	   	 
	    @FXML
	    public void switchReglesDeux(ActionEvent event) throws IOException {

			Parent tableViewParent = FXMLLoader.load(getClass().getResource("reglesDeux.fxml"));
			Scene tableViewScene = new Scene(tableViewParent);
			
			// Get the stage information
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			
			window.setScene(tableViewScene);
			window.show();
		   
	  }
	   
	    @FXML
	   public void retourAcceuil(ActionEvent event) throws IOException {

			Parent tableViewParent = FXMLLoader.load(getClass().getResource("acceuil.fxml"));
			Scene tableViewScene = new Scene(tableViewParent);
			
			// Get the stage information
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			
			window.setScene(tableViewScene);
			window.show();
		   
	  }
	    
	    
	    public void actionCase0(MouseEvent event){
	    	if(testCase0) {
		    	if(inverse) {
		    		case0.setText("X");
		    		inverse=false;
		    	}
		    	else {
		    		case0.setText("O");
		    		inverse=true;
		    	}
		    	testCase0=false;
	    	}
	    	verifGagnant();
	    	//Line line = new Line(442, 242, 442, 530);
	    	//Line line = new Line(case0.getLayoutX()+75, case0.getLayoutY()-45, 442, 530);
	        //line.setStartX(442.0f);
	        //line.setStartY(242.0f);
	        //line.setEndX(442.0f);
	        //line.setEndY(530.0f);
	        
	    	
	    }
	    
		 public void actionCase1(MouseEvent event){
			 if(testCase1) {
				 if(inverse) {
			    		case1.setText("X");
			    		inverse=false;
			    	}
			    	else {
			    		case1.setText("O");
			    		inverse=true;
			    	}
				 testCase1=false;
				 verifGagnant();
				 }
		    }
		
		 public void actionCase2(MouseEvent event){
			 if(testCase2) {
				 if(inverse) {
			    		case2.setText("X");
			    		inverse=false;
			    	}
			    	else {
			    		case2.setText("O");
			    		inverse=true;
			    	}
				 testCase2=false;
				 verifGagnant();
			 }
			 
		 }
		
		 public void actionCase3(MouseEvent event){
			 if(testCase3) {
				 if(inverse) {
			    		case3.setText("X");
			    		inverse=false;
			    	}
			    	else {
			    		case3.setText("O");
			    		inverse=true;
			    	}
				testCase3=false;
				verifGagnant();
			 }
			 
		 }
		
		 public void actionCase4(MouseEvent event){
			 if(testCase4) {
				 if(inverse) {
			    		case4.setText("X");
			    		inverse=false;
			    	}
			    	else {
			    		case4.setText("O");
			    		inverse=true;
			    	}
				 testCase4=false;
				 verifGagnant();
			 }
			 
		 }
		
		 public void actionCase5(MouseEvent event){
			 if(testCase5) {
				 if(inverse) {
			    		case5.setText("X");
			    		inverse=false;
			    	}
			    	else {
			    		case5.setText("O");
			    		inverse=true;
			    	}
				 testCase5=false;
				 verifGagnant();
			 }
			 
		 }
		
		 public void actionCase6(MouseEvent event){
			 if(testCase6) {
				 if(inverse) {
			    		case6.setText("X");
			    		inverse=false;
			    	}
			    	else {
			    		case6.setText("O");
			    		inverse=true;
			    	}
				 testCase6=false;
				 verifGagnant();
			 }
			 
		 }
		 public void actionCase7(MouseEvent event){
			 if(testCase7) {
				 if(inverse) {
			    		case7.setText("X");
			    		inverse=false;
			    	}
			    	else {
			    		case7.setText("O");
			    		inverse=true;
			    	}
				 testCase7=false;
				 verifGagnant();
			 	}
			 
		    }

		 public void actionCase8(MouseEvent event){
			 if(testCase8) {
			 if(inverse) {
		    		case8.setText("X");
		    		inverse=false;
		    	}
		    	else {
		    		case8.setText("O");
		    		inverse=true;
		    	}
			 	testCase8=false;
			 	 verifGagnant();
			 	}
			
		    }
		 
		 public void setAllFalse() {
			 testCase0=false;
			 testCase1=false;
			 testCase2=false;
			 testCase3=false;
			 testCase4=false;
			 testCase5=false;
			 testCase6=false;
			 testCase7=false;
			 testCase8=false;
		 }
		 
		 public void setAllTrue() {
			 testCase0=true;
			 testCase1=true;
			 testCase2=true;
			 testCase3=true;
			 testCase4=true;
			 testCase5=true;
			 testCase6=true;
			 testCase7=true;
			 testCase8=true;
			 case0.setText("");
			 case1.setText("");
			 case2.setText("");
			 case3.setText("");
			 case4.setText("");
			 case5.setText("");
			 case6.setText("");
			 case7.setText("");
			 case8.setText("");
		 }
		 

		 public void verifGagnant(){
			 if((case0.getText()=="X") && (case3.getText()=="X") && (case6.getText()=="X")) {
				 setAllFalse();
				 Line line = new Line(442, 235, 442, 530);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur bleu a gagn�!");
			     int a = Integer.valueOf(scoreBleu.getText());
			     scoreBleu.setText(String.valueOf(a+1));
			     reload(line);
			     
				
			     
			 }
			 else if((case1.getText()=="X") && (case4.getText()=="X") && (case7.getText()=="X")) {
				 setAllFalse();
				 Line line = new Line(597, 235, 597, 530);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur bleu a gagn�!");
			     int a = Integer.valueOf(scoreBleu.getText());
			     scoreBleu.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case2.getText()=="X") && (case5.getText()=="X") && (case8.getText()=="X")) {
				 setAllFalse();
				 Line line = new Line(750, 235, 750, 530);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur bleu a gagn�!");
			     int a = Integer.valueOf(scoreBleu.getText());
			     scoreBleu.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case0.getText()=="X") && (case1.getText()=="X") && (case2.getText()=="X")) {
				 setAllFalse();
				 Line line = new Line(442, 235, 750, 235);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur bleu a gagn�!");
			     int a = Integer.valueOf(scoreBleu.getText());
			     scoreBleu.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case3.getText()=="X") && (case4.getText()=="X") && (case5.getText()=="X")) {
				 setAllFalse();
				 Line line = new Line(442, 380, 750, 380);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur bleu a gagn�!");
			     int a = Integer.valueOf(scoreBleu.getText());
			     scoreBleu.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case6.getText()=="X") && (case7.getText()=="X") && (case8.getText()=="X")) {
				 setAllFalse();
				 Line line = new Line(442, 533, 750, 533);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur bleu a gagn�!");
			     int a = Integer.valueOf(scoreBleu.getText());
			     scoreBleu.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case0.getText()=="X") && (case4.getText()=="X") && (case8.getText()=="X")) {
				 Line line = new Line(442, 235, 750, 533);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
				 setAllFalse();
				 msgJoueur.setText("Le joueur bleu a gagn�!");
				 int a = Integer.valueOf(scoreBleu.getText());
				 scoreBleu.setText(String.valueOf(a+1));
				 reload(line);
			 }
			 else if((case2.getText()=="X") && (case4.getText()=="X") && (case6.getText()=="X")) {
				 Line line = new Line(442, 530, 750, 235);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
				 setAllFalse();
				 msgJoueur.setText("Le joueur bleu a gagn�!");
				 int a = Integer.valueOf(scoreBleu.getText());
				 scoreBleu.setText(String.valueOf(a+1));
				 reload(line);
			 }
			 
			 
			 else if((case0.getText()=="O") && (case3.getText()=="O") && (case6.getText()=="O")) {
				 setAllFalse();
				 Line line = new Line(442, 235, 442, 530);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur vert a gagn�!");
			     int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case1.getText()=="O") && (case4.getText()=="O") && (case7.getText()=="O")) {
				 setAllFalse();
				 Line line = new Line(597, 235, 597, 530);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur vert a gagn�!");
			     int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case2.getText()=="O") && (case5.getText()=="O") && (case8.getText()=="O")) {
				 setAllFalse();
				 Line line = new Line(750, 235, 750, 530);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur vert a gagn�!");
			     int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case0.getText()=="O") && (case1.getText()=="O") && (case2.getText()=="O")) {
				 setAllFalse();
				 Line line = new Line(442, 235, 750, 235);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur vert a gagn�!");
			     int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case3.getText()=="O") && (case4.getText()=="O") && (case5.getText()=="O")) {
				 setAllFalse();
				 Line line = new Line(442, 380, 750, 380);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur vert a gagn�!");
			     int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case6.getText()=="O") && (case7.getText()=="O") && (case8.getText()=="O")) {
				 setAllFalse();
				 Line line = new Line(442, 533, 750, 533);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
			     msgJoueur.setText("Le joueur vert a gagn�!");
			     int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
			     reload(line);
			 }
			 else if((case0.getText()=="O") && (case4.getText()=="O") && (case8.getText()=="O")) {
				 Line line = new Line(442, 235, 750, 533);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
				 setAllFalse();
				 msgJoueur.setText("Le joueur vert a gagn�!");
				 int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
				 reload(line);
			 }
			 else if((case2.getText()=="O") && (case4.getText()=="O") && (case6.getText()=="O")) {
				 Line line = new Line(442, 530, 750, 235);
				 line.setStrokeWidth(4);
			     line.setStroke(Paint.valueOf("RED"));
			     pane.getChildren().addAll(line);
				 setAllFalse();
				 msgJoueur.setText("Le joueur vert a gagn�!");
				 int a = Integer.valueOf(scoreVert.getText());
			     scoreVert.setText(String.valueOf(a+1));
				 reload(line);
			 }
			 else if(testCase0==false&&testCase1==false&&testCase2==false&&testCase3==false&&testCase4==false&&testCase5==false&&testCase6==false&&testCase7==false&&testCase8==false){
				 Line line = new Line(0,0,0,0);
				 msgJoueur.setText("Egalit� !");
				 reload(line);
			 }
			 
			 
			 else {
				 
			
				 if(msgJoueur.getText().equals("C'est au tour du joueur vert !")) {
					 msgJoueur.setText("C'est au tour du joueur bleu !");
				 }
				 else {
					 msgJoueur.setText("C'est au tour du joueur vert !");
				 }
					
				
					 
					 
				 
				 setColor();
			 }
			
		 }
		 
		 public void setColor() {
			 if(testCase0) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case0.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case0.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
			 if(testCase1) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case1.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case1.setFill(Paint.valueOf("#035e0c"));
				 }
			 } 
			 if(testCase2) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case2.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case2.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
			 if(testCase3) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case3.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case3.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
			 if(testCase4) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case4.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case4.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
			 if(testCase5) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case5.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case5.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
			 if(testCase6) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case6.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case6.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
			 if(testCase7) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case7.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case7.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
			 if(testCase8) {
				 if(msgJoueur.getText().equals("C'est au tour du joueur bleu !")) {
					 case8.setFill(Paint.valueOf("#114dbc"));
				 }
				 else{
					 case8.setFill(Paint.valueOf("#035e0c"));
				 }
			 }
		 }
		
		 public void reload(Line line) {
			 Thread t = new Thread() {
			      public void run() {
			    	  	try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    	  	line.setStartX(0.0f);
				        line.setStartY(0.0f);
				        line.setEndX(0.0f);
				        line.setEndY(0.0f);
				        setAllTrue();
						 if((Integer.valueOf(scoreVert.getText()) + Integer.valueOf(scoreBleu.getText())%2!=0)||(msgJoueur.getText().equals("Egalit� !"))) {
							 inverse = true;
							 msgJoueur.setText("C'est au tour du joueur bleu !");
							 setColor();
						 }
						 else {
							 msgJoueur.setText("C'est au tour du joueur vert !");
							 setColor();
						 }
						 
			      }
			 };
			 
			t.start();
		
			 
		 }
	   
		
			
}
