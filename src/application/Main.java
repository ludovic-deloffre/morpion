package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.shape.Line;


public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/application/acceuil.fxml"));
            Scene scene = new Scene(root, 1200, 800);
            
            Line line = new Line();
            line.setStartX(100.0f);
            line.setStartY(200.0f);
            line.setEndX(300.0f);
            line.setEndY(70.0f);
            line.setStrokeWidth(10);
        	  
        

            primaryStage.setScene(scene);
            primaryStage.show();
            
            
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}
